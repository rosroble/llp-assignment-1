cmake_minimum_required(VERSION 3.19.2)
project(lab1_rdb C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "-O0")

add_executable(lab1_rdb main.c table.c table.h database.c database.h data.c data.h data_iterator.c data_iterator.h util.c util.h)
